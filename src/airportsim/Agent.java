
package airportsim;

import java.util.Date;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Agent 
{
    AgentType at;
    AgentLoc al;
    Date start;
    long inUse;
    long idle;
    boolean free = true;
    enum AgentType
    {
        Coach, FirstClass
    }
    enum AgentLoc
    {
        CheckIn, Security
    }
    public Agent(AgentType agent, AgentLoc loc, Date Now)
    {
        at = agent;
        al = loc;
        start = Now;
        inUse = 0;
        idle = 0;
    }
    
    public Date addUseTime(long time, Date Now)
    {
        
        inUse += time;
        
        return new Date(Now.getTime() + time);
    }
    
    
    public double getUtilization(Date end)
    {
        return (double)inUse / (double)(end.getTime() - start.getTime());
    }
    public void Use()
    {
        free = false;
    }
    public void Free()
    {
        free = true;
    }
    public boolean isFree()
    {
        return free;
    }
    public AgentType getType()
    {
        return at;
    }

}
