
package airportsim;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;
/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class AirportSim {

    /**
     * @param args the command line arguments
     */
   
    public static void main(String[] args) throws FileNotFoundException {
        
        //Parameters
        long MillisBWIntlFlights = 6*60*60*1000;
        long MillisBWCommFlights = 55*60*1000;
        long MillisInHour = 60*60*1000;
        int commCap = 50;
        int intlCap = 200;
        long SimTime = (long)(1 * 7 * 24 * MillisInHour) + 1;
        int CoachCheckinAgents = 7;
        int FirstClassCheckinAgents =2;
        int CoachSecAgents = 4;
        int FirstClassSecAgents = 1;
        String startDateS = "01/01/2000 00:00";
        //
        
        PrintWriter file = new PrintWriter("EventLog.csv");
        PrintWriter stat = new PrintWriter("Stats.csv");
        file.println("Action,Time,Type,Other");
        PriorityQueue<Event> el = new PriorityQueue();
        List<Event> events = new ArrayList<>();
        
        


        
        //Get dates

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
        Date startDate = new Date(0);
        try {
            startDate = df.parse(startDateS);
            String newDateString = df.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
         }
        Date timer = new Date(startDate.getTime());
        //Date intlFlight = new Date(startDate.getTime() + MillisBWIntlFlights);
       
        
        //Add agents
        List<Agent> ciCoach = new ArrayList<>();
         for(int i = 0; i < CoachCheckinAgents; i++)
        {
            ciCoach.add(new Agent(Agent.AgentType.Coach, Agent.AgentLoc.CheckIn, startDate));
        }
         List<Agent> ciFC = new ArrayList<>();
        for(int i = 0; i < FirstClassCheckinAgents; i++)
        {
            ciFC.add(new Agent(Agent.AgentType.FirstClass, Agent.AgentLoc.CheckIn, startDate));
        }
         List<Agent> sCoach = new ArrayList<>();
         for(int i = 0; i < CoachSecAgents; i++)
        {
            sCoach.add(new Agent(Agent.AgentType.Coach, Agent.AgentLoc.Security, startDate));
        }
         List<Agent> sFC = new ArrayList<>();
        for(int i = 0; i < FirstClassSecAgents; i++)
        {
            sFC.add(new Agent(Agent.AgentType.FirstClass, Agent.AgentLoc.Security, startDate));
        }
        //Add planes
        List<Plane> cPlanes = new ArrayList<>();
        for(long t = startDate.getTime() + (long)(0.5*MillisInHour); t <= startDate.getTime() + SimTime; t+= MillisBWCommFlights)
        {
            cPlanes.add(new Plane(Plane.planeType.commuter,commCap, new Date(t)));
        }
        List<Plane> iPlanes = new ArrayList<>();
        for(long t = startDate.getTime() + MillisBWIntlFlights; t <= startDate.getTime() + SimTime; t+= MillisBWIntlFlights)
        {
            iPlanes.add(new Plane(Plane.planeType.intl,intlCap, new Date(t)));
        }
        
        List<Passenger> cPass = new ArrayList<>();
        List<Passenger> ICPass = new ArrayList<>();
        List<Passenger> IFPass = new ArrayList<>();
        
        //Setup modules
        int arrivals = 0;
        int checkins = 0;
        int missed = 0;
        int security = 0;
        int depart = 0;
        
        Arrival a = new Arrival(el);
        Checkin c = new Checkin(el,ciCoach, ciFC);
        Security s = new Security(el, sCoach, sFC);
        Gate g = new Gate(el, cPlanes, iPlanes);
        Ledger l = new Ledger();
        l.setNumAgents(CoachCheckinAgents + FirstClassCheckinAgents + CoachSecAgents + FirstClassSecAgents);
        
        //Get customers
        a.commuterArrival(timer);
        for(Plane p : iPlanes)
        {
            a.intlArrival(p.getDepartTime());
        }
        
        
        //Start Simulation
        while(timer.before(new Date(startDate.getTime() + SimTime)))
        {
            Event next = el.remove();
           // events.add(next);
            Passenger p = next.getPassenger();
            //Check if pass missed flight
            if(!p.getType().equals(Passenger.pType.commuter))
            {
                if(p.getFlightTime().before(timer))
                {
                    //Missed flight
                    next = new Event(timer, timer, p, "Missed Flight");
                    //l.checkRefund(p);
                    if(p.getAgent() != null)
                    {
                        p.getAgent().Free();
                    }
                }
            }
            //Handle next event
            if(next.eventName().equals("Arrival"))
            {
                if(p.getType().equals(Passenger.pType.commuter))
                {
                    a.commuterArrival(timer);
                    cPass.add(p);
                }
                else if(p.getType().equals(Passenger.pType.intlCH))
                {
                     ICPass.add(p);
                }
                else
                {
                    IFPass.add(p);
                }
                l.addPassengerTicket(p);
                arrivals ++;
                timer = next.eventTime();
                c.CheckInLine(p, timer);
            }
            else if(next.eventName().equals("Check-in"))
            {
                c.getNextCustomer(p.getAgent(), timer);
                p.setServiceAgent(null);
                checkins++;
                timer = next.eventTime();
                s.SecurityLine(p, timer);
            }
            else if(next.eventName().equals("Security"))
            {
                s.getNextPassenger(p.getAgent(), timer);
                p.setServiceAgent(null);
                security++;
                timer = next.eventTime();
                g.Embark(p, timer);
            }
            else if(next.eventName().equals("Depart"))
            {
                depart++;
                Plane pl = p.getPlane();
                if(timer.before(pl.getDepartTime()))
                {
                    l.addFlight(pl);
                    ReportPlane(file, pl);
                }
                
                 
                
                timer = next.eventTime();
                

                
            }
            else if(next.eventName().equals("Missed Flight"))
            {
                missed++;
                timer = next.eventTime();
                l.checkRefund(p);
            }
            //Report
            ReportEvent(file, next);
        }
        System.out.println("Arrivals " + arrivals);
        System.out.println("Check-ins " + checkins);
        System.out.println("Security " + security);
        System.out.println("Departures " + depart);
        System.out.println("Missed flights " + missed);
        /*
        System.out.println("Commuter arrivals in 1 hour: " + arrivals);
        System.out.println("Intl arrivals for Intl Flight at " + intlFlight.toString() + ":");
        int p = 0;
        for(Event e : el)
        {
            if(!e.p.getType().equals(Passenger.pType.commuter))
            {
                p++;
                System.out.println(e.eventTime().toString());
            }
        }
        System.out.println("Plane capacity = " + p + "/200");
        */ 
        l.ReportLedger(stat, SimTime);
        //ReportEventStats(stat,events);
        ReportPlaneStats(stat,cPlanes,iPlanes);
        ReportPassengerStats(stat,cPass,ICPass,IFPass);
        ReportAgentStats(stat,timer,ciCoach,ciFC,sCoach,sFC);
        stat.close();
        file.close();
    }
    
    public static void ReportEvent(PrintWriter w, Event e)
    {
        String s = "Event,";
        s += e.eventTime() + ",";
        Passenger p = e.getPassenger();
        if(p.getType().equals(Passenger.pType.commuter))
        {
            s+= "Commuter Passenger,";
        }
        else if(p.getType().equals(Passenger.pType.intlCH))
        {
            s+= "Intl. Passenger (Coach),";
        }
        else
        {
            s+= "Intl. Passenger (First Class),";
        }
        
        s+= e.eventName() ;
        
        w.println(s);
       // System.out.println(s);
    }
    public static void ReportPlane(PrintWriter w, Plane p)
    {
        String s = "Plane,";
        s+= p.getDepartTime() + ",";
        if(p.getType().equals(Plane.planeType.commuter))
        {
            s += "Commuter Flight,";
        }
        else
        {
            s+= "International Flight,";
        }
        s += p.getNumPassengers() + " Passengers," + p.getRemSeats() + " Empty Seats";
        //System.out.println(s);
        w.println(s);
    }
    private static void ReportPlaneStats(PrintWriter w, List<Plane> cPl, List<Plane> iPl )
    {
        double cFilled = 0;
        double iFilled = 0;
        
        double cEmpty = 0;
        double iEmpty = 0;
        
        for(Plane p : cPl)
        {
            cFilled += p.getNumPassengers();
            cEmpty += p.getRemSeats();
        }
       for(Plane p : iPl)
        {
            iFilled += p.getNumPassengers();
            iEmpty += p.getRemSeats();
        }
       
       double cTotal = cFilled + cEmpty;
       double iTotal = iFilled + iEmpty;
       
       double nC = cPl.size();
       double nI = iPl.size();
       
       double cAempty = cEmpty / nC;
       double cAfilled = cFilled / nC;
       double cPFilled = cFilled / cTotal;
       
       double iAempty = iEmpty / nI;
       double iAfilled = iFilled / nI;
       double iPFilled = iFilled / iTotal;
       
       w.println();
       w.println("Plane Stats");
       w.println("Plane Type,Number,Avg. Filled,Avg. Empty,% Seats Filled");
       w.println("Commuter," + nC + "," + cAfilled + "," + cAempty +"," + 100*cPFilled +"%");
       w.println("International," + nI + "," + iAfilled + "," + iAempty +"," + 100*iPFilled +"%");
       
    }
    private static void ReportAgentStats(PrintWriter w, Date end, List<Agent> CCag, List<Agent> FCag, List<Agent> CSag, List<Agent> FSag)
    {
        double CCuse = 0;
        double FCuse = 0;
        double CSuse = 0;
        double FSuse = 0;
        
        for(Agent a : CCag)
        {
            CCuse += a.getUtilization(end);
        }
        for(Agent a : FCag)
        {
            FCuse += a.getUtilization(end);
        }
        for(Agent a : CSag)
        {
            CSuse += a.getUtilization(end);
        }
        for(Agent a : FSag)
        {
            FSuse += a.getUtilization(end);
        }
        
        double tAg = CCag.size() + FCag.size() + CSag.size() + FSag.size();
        double tUse = CCuse + FCuse + CSuse + FSuse;
        
        CCuse /= (double)CCag.size();
        FCuse /= (double)FCag.size();
        CSuse /= (double)CSag.size();
        FSuse /= (double)FSag.size();
        tUse /= tAg;
        
        w.println();
        w.println("Agent,Number,Avg. Utilization");
        w.println("Coach Check-in," + CCag.size() +"," + CCuse*100 + "%");
        w.println("First Class Check-in," + FCag.size() +"," + FCuse*100 + "%");
        w.println("Coach Security," + CSag.size() +"," + CSuse*100 + "%");
        w.println("First Class Security," + FSag.size() +"," + FSuse*100 + "%");
        w.println("Total," + tAg +"," + tUse*100 + "%");
        
        
    }
    private static void ReportEventStats(PrintWriter w, List<Event> evs)
    {
        long chkT = 0;
        long chkLT = 0;
        long secT = 0;
        long gateT = 0;
        long chkN = 0;
        long chkLN = 0;
        long secN = 0;
        long gateN = 0;
        
        double chkA;
        double secA;
        double gateA;
        for(Event e : evs)
        {
            if(e.eventName().equals("Check-in"))
            {
                chkT += e.eventDuration();
                chkN++;
            }
            else if(e.eventName().equals("Check-in Line"))
            {
                chkLT += e.eventDuration();
                chkLN++;
            }
            else if(e.eventName().equals("Security"))
            {
                secT += e.eventDuration();
                secN++;
            }
            else if (e.eventName().equals("Depart"))
            {
                gateT += e.eventDuration();
                gateN++;
            }
        }
        
        chkA = (double)chkT / (double)chkN;
        secA = (double)secT / (double)secN;
        gateA = (double)gateT/(double)gateN;
        
        w.println();
        w.println("Event Stats");
        w.println("Event,Number,Avg. Duration");
        w.println("Check-in," + chkN + "," + millisToString((long)chkA) );
        w.println("Security," + secN + "," + millisToString((long)secA) );
        w.println("Gate," + gateN + "," + millisToString((long)gateA) );
        
    }
    private static void ReportPassengerStats(PrintWriter w, List<Passenger> CoPass,List<Passenger> ICPass, List<Passenger> IFPass)
    {
        double passST = 0;
        double coPassST = 0;
        double ICPassST = 0;
        double IFPassST = 0;
        
        double ICmiss = 0;
        double IFmiss = 0;
        double Tmiss;
        
        double tPass;
        
        for(Passenger p : CoPass)
        {
           long t = p.getSystemTime();
           if(t >= 0)
           {
               coPassST += t;
               passST += t;
           }
        }
        
        for (Passenger p : ICPass)
        {
           long t = p.getSystemTime();
           if(t >= 0)
           {
               passST += t;
               ICPassST += t;
           }
           else
           {
               ICmiss++;
           }
        }
        
         for (Passenger p : IFPass)
        {
           long t = p.getSystemTime();
           if(t >= 0)
           {
               passST += t;
               IFPassST += t;
           }
           else
           {
               IFmiss++;
           }
        }
         
         tPass = CoPass.size() + ICPass.size() + IFPass.size() - ICmiss - IFmiss;
         
         passST /= tPass;
         coPassST /= (double)CoPass.size();
         ICPassST /= (double)(ICPass.size() - ICmiss);
         IFPassST /= (double)(IFPass.size() - IFmiss);
         
         Tmiss = (ICmiss + IFmiss) / (double)(ICPass.size() + IFPass.size());
         ICmiss /= (double)ICPass.size();
         IFmiss /= (double)IFPass.size();
         
         
         w.println();
         w.println("Passenger Stats");
         w.println("Passanger Type,Number,Avg. System Time,% Missed Flights");

         
         w.println("Commuter," + CoPass.size() + ","+ millisToString((long)coPassST) + ",N/A");
         w.println("Intl. Coach," + ICPass.size() + "," + millisToString((long)ICPassST) + "," + (ICmiss * 100) +"%");
         w.println("Intl. First Class," + IFPass.size() + "," + millisToString((long)IFPassST) + "," + (IFmiss * 100) +"%");
         w.println("Totals," + tPass + "," + millisToString((long)passST) + "," + (Tmiss * 100) +"%");
        
    }
    
    public static String millisToString(long millis)
    {
        return String.format("%02d:%02d:%02d", 
        TimeUnit.MILLISECONDS.toHours(millis),
        TimeUnit.MILLISECONDS.toMinutes(millis) -  
        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
        TimeUnit.MILLISECONDS.toSeconds(millis) - 
        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );   
    }
    
   

}
