
package airportsim;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.Random;
import org.apache.commons.math3.distribution.*;
/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Arrival 
{
    PriorityQueue el;
    public Arrival(PriorityQueue EventList)
    {
        el = EventList;
        
    }
    
    public void commuterArrival(Date Now)
    {
        int SecInMillis = 1000;
        double mean = (60.0*60.0*SecInMillis)/40.0;  //40 people per hour in ms
        
        //Get next passenger arrival
        PoissonDistribution pd = new PoissonDistribution(mean);
        Date arr;
        arr = new Date(Now.getTime() + pd.sample());
        //Create a new passenger
        Passenger p = new Passenger(arr, Passenger.pType.commuter);
        giveBags(p);
        //Create a new event
        Event e = new Event(Now, arr, p, "Arrival");
        //Add to list
        el.add(e);
        
    }
    public void intlArrival(Date NextFlight)
    {
        int MinInMillis = 60*1000;
        double mean = 75 * MinInMillis; //75 mins
        double sd = 50 * MinInMillis;
        
        //get # of coach
        Random r = new Random();
        int coachPass = 0;
        double coachP = 0.85;
        int coachSeats = 150;
        for(int i = 0; i < coachSeats; i++)
        {
            if(r.nextDouble() <= coachP)
            {
                coachPass ++;
            }
        }
        
        //get # of first class
        int fcPass = 0;
        double fcP = 0.80;
        int fcSeats = 50;
        for(int i = 0; i < fcSeats; i++)
        {
            if(r.nextDouble() <= fcP)
            {
                fcPass++;
            }
        }
        
        NormalDistribution nd = new NormalDistribution(mean, sd);
        Date arr;
        //get coach arrival times
        for(int i = 0; i < coachPass; i++)
        {
            arr = new Date(NextFlight.getTime() - (long)(nd.sample()));
            Passenger p = new Passenger(arr, NextFlight, Passenger.pType.intlCH);
            giveBags(p);
            Event e = new Event(new Date(0), arr, p, "Arrival");
            el.add(e);
        }
        //get FC arrival times
        for(int i = 0; i < fcPass; i++)
        {
            arr = new Date(NextFlight.getTime() - (long)(nd.sample()));
            Passenger p = new Passenger(arr, NextFlight, Passenger.pType.intlFC);
            giveBags(p);
            Event e = new Event(new Date(0), arr, p, "Arrival");
            el.add(e);
        }
    }
    public void giveBags(Passenger p)
    {
        double pBagsC = 0.8;
        double pBagsI = 0.6;
        int bags = 0;
        Random r = new Random();
        double res = r.nextDouble();
        if(p.getType().equals(Passenger.pType.commuter))
        {
            while(res > pBagsC)
            {
                bags++;
                res = r.nextDouble();
            }
        }
        else
        {
            while(res > pBagsI)
            {
                bags++;
                res = r.nextDouble();
            }
        }
        
        p.setBags(bags);
        
    }

}
