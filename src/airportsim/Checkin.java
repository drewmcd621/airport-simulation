
package airportsim;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import org.apache.commons.math3.distribution.*;
/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Checkin 
{
    int fcag;
    int cag;
    PriorityQueue<Event> el;
    Queue<Passenger> Cline;
    Queue<Passenger> FCline;
    List<Agent> cagL = new ArrayList<>();
    List<Agent> fcagL = new ArrayList<>();
    
    public Checkin(PriorityQueue<Event> EventList, List<Agent> CoachAgents, List<Agent> FCAgents)
    {
        el = EventList;
        cagL = CoachAgents;
        fcagL = FCAgents;
        Cline = new LinkedList<>();
       FCline = new LinkedList<>();

    }
    
    public void CheckInLine(Passenger p, Date Now)
    {

        //Find next free agent
        Agent next;
        if(p.getType().equals(Passenger.pType.intlFC))
        {
           next = getFreeAgent(fcagL);
        }
        else
        {
            next = getFreeAgent(cagL);
        }
        if(next != null)
        {
            p.setServiceAgent(next);
            next.Use();
            CheckIn(p, Now);
        }
        else
        {
            if(p.getType().equals(Passenger.pType.intlFC))
            {
                FCline.add(p);
            }
            else
            {
                Cline.add(p);
            }
        }
        
    }
    private void CheckIn(Passenger p, Date Now)
    {
        Agent a = p.getAgent();
        //Get service time
        long servetime = printPass() + checkBags(p) + delay();
         Date time = a.addUseTime(servetime, Now);
        
        Event e = new Event(Now, time, p, "Check-in");  
        
        el.add(e);
    }
    public void getNextCustomer(Agent a, Date Now)
    {
        Passenger p;
        if(a.getType() == Agent.AgentType.FirstClass)
        {
            p = FCline.poll();
        }
        else
        {
            p = Cline.poll();
        }
        
        if(p == null)
        {
            a.Free();
        }
        else
        {
            p.setServiceAgent(a);
            CheckIn(p, Now);
        }
    }
    
    private Agent getFreeAgent(List<Agent> agL)
    {
         
         for(Agent a : agL)
         {
                if(a.isFree()) return a;
         }
         return null;
    }
    
    
    private long printPass()
    {
        double MinInMillis = 60*1000;
        ExponentialDistribution ex = new ExponentialDistribution(2*MinInMillis);
        return (long)ex.sample();   
    }
    private long checkBags(Passenger p)
    {
        int bags = p.getBags();
        double MinInMillis = 60*1000;
        ExponentialDistribution ex = new ExponentialDistribution(MinInMillis);
        long time = 0;
        for(int b = 0; b < bags; b++)
        {
            time += (long)ex.sample();
        }
        return time;
    }
    private long delay()
    {
        double MinInMillis = 60*1000;
        ExponentialDistribution ex = new ExponentialDistribution(3*MinInMillis);
        return (long)ex.sample();   
    }

}
