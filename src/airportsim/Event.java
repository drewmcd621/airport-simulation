
package airportsim;

import java.util.Date;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Event implements Comparable<Event> 
{
    Date eTime;
    Date sTime;
    Passenger p;
    String act;
    
    public Event(Date startTime, Date eventTime, Passenger person, String activity)
    {
        eTime = eventTime;
        sTime = startTime;
        p = person;
        act = activity;
    }

    @Override
    public int compareTo(Event o) 
    {
        return eTime.compareTo(o.eTime);
        
    }
    public String eventName()
    {
        return act;
    }
    public Date eventTime()
    {
        return eTime;
    }
    public Passenger getPassenger()
    {
        return p;
    }
    
    public long eventDuration()
    {
        return eTime.getTime() - sTime.getTime();
    }

}
