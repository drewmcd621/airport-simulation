
package airportsim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Gate 
{
    List<Plane> cDep;
    List<Plane> iDep;
    PriorityQueue el;
    
    public Gate(PriorityQueue EventList,List<Plane> CommuterPlanes,List<Plane> IntlPlanes)
    {
        el = EventList;
        cDep = CommuterPlanes;
        iDep = IntlPlanes;
    }
    
    public void Embark(Passenger p, Date Now)
    {
        Plane pl;
        if(p.getType().equals(Passenger.pType.commuter))
        {
            pl = getNextCommPlane(Now);
        }
        else
        {
            pl = getIntlFlight(p, Now);
        }
        if(pl != null)
        {
            pl.addPassenger(p);
            p.setPlane(pl);
            Event e = new Event(Now, pl.getDepartTime(), p, "Depart");
            el.add(e);
        }
    }
    private Plane getNextCommPlane(Date Now)
    {
        //Go through planes until it is after the date
        Collections.sort(cDep);
        for(Plane p : cDep)
        {
            if(p.getDepartTime().after(Now))
            {
                if(p.getRemSeats() >= 1)
                {
                    return p;
                }
            }
        }
        return null;
    }
    
    private Plane getIntlFlight(Passenger pass, Date Now)
    {
        Date depart = pass.getFlightTime();
        for(Plane p : iDep)
        {
            if(p.getDepartTime().equals(depart))
            {
                if(p.getDepartTime().after(Now))
                {
                    return p;
                }
            }
        }
        return null;
    }
    
}
