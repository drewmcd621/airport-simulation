
package airportsim;

import airportsim.Passenger.pType;
import airportsim.Plane.planeType;
import java.io.PrintWriter;
import java.text.DecimalFormat;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Ledger 
{
    //Ledger types
    int commPlanes = 0;
    int intlPlanes = 0;
    int fcTickets = 0;
    int chTickets = 0;
    int coTickets = 0;
    int fcRefunds = 0;
    int chRefunds = 0;
    int agents = 0;
    long agentHours = 0;
    
    //Ledger costs
    int commPlaneCost = 1000;
    int intlPlaneCost = 10000;
    int fcTicketCost = 1000;
    int chTicketCost = 500;
    int coTicketCost = 200;
    int agentCost = 25;
    
    //Other
    long refundTime = 90*60*1000; //90 mins
    
    public Ledger()
    {
        
    }
    
    public void addPassengerTicket(Passenger p)
    {
        Passenger.pType pt = p.getType();
        if(pt == pType.commuter)
        {
            coTickets++;
        }
        else if(pt == pType.intlCH)
        {
            chTickets++;
        }
        else
        {
            fcTickets++;
        }
    }
    
    public void checkRefund(Passenger p)
    {
        if(p.getTimeBeforeFlight() >= refundTime)
        {
            //Do refund
            if(p.getType() == pType.intlFC)
            {
                fcRefunds++;
            }
            else
            {
                chRefunds++;
            }
            
        }
    }
    public void addFlight(Plane p)
    {
        if(p.getType() == planeType.commuter)
        {
            commPlanes++;
        }
        else
        {
            intlPlanes++;
        }
    }
    public void setNumAgents(int a)
    {
        agents = a;
    }
    
    public void ReportLedger(PrintWriter w, long SimTime)
    {
        double HoursInMillis = 60*60*1000;
        
        double total;
        double cTotal = 0;
        double dTotal = 0;
        double nProfit = 0;
        
        w.println();
        w.println("Ledger");
        w.println("Item Name,Cost,Qty,Total");
        w.println("Credits");
        //Tickets
        total = fcTickets*fcTicketCost;
        cTotal += total;
        w.println("Intl. Flight Ticket (First Class)," + (fcTicketCost) + "," + fcTickets + "," +(total) );
        total = chTickets*chTicketCost;
        cTotal += total;
        w.println("Intl. Flight Ticket (Coach)," + (chTicketCost) + "," + chTickets + "," +(total) );
         total = coTickets*coTicketCost;
         cTotal += total;
        w.println("Commuter Flight Ticket," + (coTicketCost) + "," + coTickets + "," +(total) );
        w.println("Credits Total,,," + (cTotal));
       //Debits
        w.println("Debits");
        //Planes
        total = intlPlanes * intlPlaneCost;
        dTotal += total;
        w.println("Intl. Plane," + (-1*intlPlaneCost) + "," + intlPlanes + "," +(-1*total) );
        total = commPlanes * commPlaneCost;
        dTotal += total;
        w.println("Commuter Plane," + (-1*commPlaneCost) + "," + commPlanes + "," +(-1*total) );
        //Agents
        double qty = agents*(SimTime / HoursInMillis);
        total = qty * agentCost;
        dTotal += total;
        w.println("Employees," + (-1*agentCost) + "," + qty + "," +(-1*total) +",(" + agents + " Employees)" );
        //Refunds
        total = fcRefunds*fcTicketCost;
        dTotal += total;
        w.println("First Class Ticket Refunds," + (-1*fcTicketCost) + "," + fcRefunds + "," +(-1*total));
        total = chRefunds*chTicketCost;
        dTotal += total;
        w.println("Coach Ticket Refunds," + (-1*chTicketCost) + "," + chRefunds + "," +(-1*total));
        
        w.println("Debit Total,,," + (-1*dTotal));
        nProfit = cTotal - dTotal;
        w.println("Net Profit,,," + (nProfit));
        double days = SimTime / (24*HoursInMillis);
        double dProfit = nProfit / days;
        w.println("Avg. Daily Profit,," + days + "," + dProfit);

    }
    

}
