
package airportsim;

import java.util.Date;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Passenger {
    enum pType
{
commuter, intlCH, intlFC
}
    
    Date arr = new Date(0);
    Date flight;
    Date depar = new Date(0);
    Plane pl;
    pType pt;
    int bags;
    Agent s;

    public Passenger(Date arrival, pType passengerType)
    {
        arr = arrival;
        pt = passengerType;
    }
    public Passenger(Date arrival, Date flightDeparture, pType passengerType)
    {
        arr = arrival;
        flight = flightDeparture;
        pt = passengerType;  //Should be an Intl variant in this case
    }
    
    public void setBags(int b)
    {
        bags = b;
    }
    public int getBags()
    {
        return bags;
    }
    
    public pType getType()
    {
        return pt;
    }
    
    public Date getFlightTime()
    {
        return flight;
    }
    public void setDepart(Date time)
    {
        depar = time;
    }
    public Plane getPlane()
    {
        return pl;
    }
    public void setPlane(Plane p)
    {
        pl = p;
    }
    public long getSystemTime()
    {
        
        long time = depar.getTime() - arr.getTime();
        if(time < 0) return -1;
        return time;
        
    }
    public long getTimeBeforeFlight()
    {
        return flight.getTime() - arr.getTime();
    }    
    public void setServiceAgent(Agent serv)
    {
        s = serv;
    }
  
    
    public Agent getAgent()
    {
        return s;
    }

}


