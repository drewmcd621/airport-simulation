
package airportsim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Plane implements Comparable<Plane>
{
    int cap;
    int pass;
    Date flTime;
    List<Passenger> pL;

    @Override
    public int compareTo(Plane o) 
    {
        return this.flTime.compareTo(o.flTime);
    }
    
    enum planeType
    {
        commuter, intl
    }
    
    planeType pt;
    
    public Plane(planeType type, int capacity, Date depart)
    {
        cap = capacity;
        pt = type;
        pL = new ArrayList<>();
        flTime = depart;
        
        
    }
    public int addPassenger(Passenger p)
    {
        if(getRemSeats() >= 1)
        {
            pL.add(p);
            pass++;
            p.setDepart(flTime);
            return 0;
        }
        return -1;
    }
    public int getRemSeats()
    {
        return cap - pass;
    }
    public int getNumPassengers()
    {
        return pass;
    }
    public Date getDepartTime()
    {
        return flTime;
    }
    public planeType getType()
    {
        return pt;
    }
}
