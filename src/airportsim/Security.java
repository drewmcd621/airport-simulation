
package airportsim;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import org.apache.commons.math3.distribution.*;
/**
 *
 * Drew McDermott
 * adm75@pitt.edu
 */
public class Security 
{
    List<Agent> cSec;
    List<Agent> fcSec;
   Queue<Passenger> Cline;
    Queue<Passenger> FCline;
    
    PriorityQueue el;
    
    public Security(PriorityQueue EventList, List<Agent> CoachSecurity, List<Agent> FCSecurity)
    {
       cSec = CoachSecurity;
       fcSec = FCSecurity;
       el = EventList;
       Cline = new LinkedList<>();
       FCline = new LinkedList<>();
    }
    
    private long screenTime()
    {
        double MinInMillis = 60*1000;
        ExponentialDistribution ex = new ExponentialDistribution(3*MinInMillis);
        return (long)ex.sample();
    }
    
     private Agent getFreeAgent(List<Agent> agL)
    {
         
         for(Agent a : agL)
         {
                if(a.isFree()) return a;
         }
         return null;
    }
     
    public void Security(Passenger p, Date Now)
    {
        Agent a = p.getAgent();
        //Get service time
        long serviceTime = screenTime();
        
        //"Reserve" a spot for use of this agent
        Date time = a.addUseTime(serviceTime, Now);
        
        Event e = new Event(Now, time, p, "Security");
        
        el.add(e);
        
    }
    public void SecurityLine(Passenger p, Date Now)
    {
          //Find next free agent
        Agent next;
        if(p.getType().equals(Passenger.pType.intlFC))
        {
           next = getFreeAgent(fcSec);
        }
        else
        {
            next = getFreeAgent(cSec);
        }
        if(next != null)
        {
            p.setServiceAgent(next);
            next.Use();
            Security(p, Now);
        }
        else
        {
            if(p.getType().equals(Passenger.pType.intlFC))
            {
                FCline.add(p);
            }
            else
            {
                Cline.add(p);
            }
        }
              
    }
    
    public void getNextPassenger(Agent a, Date Now)
    {
        Passenger p;
        if(a.getType() == Agent.AgentType.FirstClass)
        {
            p = FCline.poll();
        }
        else
        {
            p = Cline.poll();
        }
        
        if(p == null)
        {
            a.Free();
        }
        else
        {
            p.setServiceAgent(a);
            Security(p, Now);
        }
    }
        
}
